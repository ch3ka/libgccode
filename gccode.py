#!/usr/bin/env python3

''' libgccode - a library for gccodes, a compact and UX-friendly identifier encoding.

Provides functions to transform an integer (mainly an id) to base31 and back.
base31 is a special encoding invented by Groundspeak, which is basically base36 with
some characters removed due to looking similar to others. As a side effect, this also 
drastically limits the chance of potentially "offensive" encodings.
While Groundspeak already maps 'O' to '0', we also map some other chars.
This breaks compatibility, but improves UX (at the cost of potentially legal "offensive"
codes, but they won't be offensive when normalized).
Note that compatibility with Groundspeak is not given anyway, since they use offsets (and 
used other encodings in the old days, and those are still valid).

The codes are intended to be prefixed with a model identifier you can dispatch on, so 
http://example.com/application/get_stuff?model_instance=instance.id
can become
http://example.com/application/get/MIGCODE
where MI encodes the model instance, and GCODE encodes the instance id.
'''

import string

__author__ = "^.^@rettet-die-b.org"
__email__ = __author__
__license__ = "LGPL"
__status__ = "Prototype"


_MAP = {'I' : '1', 'O': '0', 'S': '5', 'U': 'V'}
_MAPPED_CHARS = ''.join(_MAP.keys())
_EXCLUDED_CHARS = _MAPPED_CHARS + "L"

_ALPHABET = string.digits + ''.join(c for c in string.ascii_uppercase if c not in _EXCLUDED_CHARS)
_BASE = len(_ALPHABET)

def sanitize_gc(gc):
    """ normalizes gc to upper letters and maps mapped chars """
    gc = gc.upper().strip()
    return gc.translate({ord(k): v for k,v in _MAP.items()})

def id_to_gc(id_):
    """ integer (or string representation thereof) -> gc """
    try:
        id_=int(id_)
        if id_ < 0 : raise ValueError()
    except ValueError:
        raise ValueError("invalid literal for id_to_gc(): %s" % repr(id_))
    return _baseN(id_)
        
def gc_to_id(gc):
    """ gc -> integer """
    try:
        gc = sanitize_gc(gc)
        if not all(c in _ALPHABET for c in gc): raise AttributeError()
    except AttributeError:
        raise ValueError("invalid literal for gc_to_id(): %s" % repr(gc))
    return int(gc.translate(dict(zip(map(ord, _ALPHABET), string.digits+string.ascii_letters))), _BASE)

def _baseN(n, b=_BASE, a=_ALPHABET): # here be dragons. stay away.
    return (not n and a[0]) or (_baseN(n // b, b, a).lstrip(a[0]) + a[n % b])
